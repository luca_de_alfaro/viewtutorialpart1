package com.dealfaro.luca.viewtutorialpart1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

// Code is from http://www.jayway.com/2012/06/25/creating-custom-android-views-part-1-extending-standard-views-and-adding-new-xml-attributes/

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Model model = new Model();

        DurationTextView durationView1 = (DurationTextView) findViewById(R.id.total);
        durationView1.setDuration(model.getTotalDuration());

        DurationTextView durationView2 = (DurationTextView) findViewById(R.id.walking);
        durationView2.setDuration(model.walkingDuration);

        DurationTextView durationView3 = (DurationTextView) findViewById(R.id.running);
        durationView3.setDuration(model.runningDuration);

        DurationTextView durationView4 = (DurationTextView) findViewById(R.id.cycling);
        durationView4.setDuration(model.cyclingDuration);

        DistanceTextView distanceView = (DistanceTextView) findViewById(R.id.distance);
        distanceView.setDistance(model.totalDistance);
    }

    class Model {
        int walkingDuration = 2453;
        int runningDuration = 20314;
        int cyclingDuration = 12012;
        int totalDistance = 38173;

        int getTotalDuration() {
            return walkingDuration + runningDuration + cyclingDuration;
        }
    }


}
